package model;

import Utils.*;

import java.io.FileNotFoundException;
import java.time.LocalTime;
import java.util.*;

public class Metro
{
    private Graph<Station,Trip> metroGraph = new Graph<>(true); // directed = true;
    private HashMap<String, Line> lines = new HashMap<>();
    private HashMap<String, Station> allStations = new HashMap<>();

    public Metro()
    {
    }

    public Metro(String coordinatesFile, String linesStationsFile, String connectionsFile)
    {
        readData(coordinatesFile, linesStationsFile, connectionsFile);
    }

    public Graph<Station, Trip> getMetroGraph() {
        return metroGraph;
    }

    public HashMap<String, Line> getLines() {
        return lines;
    }

    public HashMap<String, Station> getAllStations() {
        return allStations;
    }

    public void readData(String coordinatesFile, String linesStationsFile, String connectionsFile)
    {
        FileReader reader = new FileReader(coordinatesFile, linesStationsFile, connectionsFile);
        reader.a1ReadFiles(this.metroGraph, this.allStations, this.lines);
    }

    public Trip shortestTripTime(Station origin, Station destination, LocalTime initialTime, boolean weighted)
    {
        LinkedList<Station> path = new LinkedList<>();
        GraphAlgorithms.shortestPath(this.metroGraph, origin, destination, path, weighted);
        Trip totalTrip = new Trip(initialTime);
        for(int i = 0; i+1 < path.size(); i++)
        {
            Trip currTrip = this.metroGraph.getEdge(path.get(i), path.get(i+1)).getElement();
            totalTrip.concatTrip(currTrip);
        }
        return totalTrip;
    }

    public Trip shortestTripTimeLines(Station origin, Station destination, LocalTime initialTime)
    {
        LinkedList<Station> path = new LinkedList<>();
        MetroAlgorithms.shortestPathLines(this, origin, destination, path);
        Trip totalTrip = new Trip(initialTime);
        for(int i = 0; i+1 < path.size(); i++)
        {
            Trip currTrip = this.metroGraph.getEdge(path.get(i), path.get(i+1)).getElement();
            totalTrip.concatTrip(currTrip);
        }
        return totalTrip;
    }

    public Trip shortestTripTimeIntermediates(Station origin, Station destination, ArrayList<Station> intermediates, LocalTime initialTime)
    {
        LinkedList<Station> path = new LinkedList<>();
        GraphAlgorithms.travellingSalesman(this.metroGraph,origin,destination,intermediates,path);
        Trip totalTrip = new Trip(initialTime);
        for(int i = 0; i+1 < path.size(); i++)
        {
            Trip currTrip = this.metroGraph.getEdge(path.get(i), path.get(i+1)).getElement();
            totalTrip.concatTrip(currTrip);
        }
        return totalTrip;
    }
}
