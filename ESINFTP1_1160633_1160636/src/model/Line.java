package model;

import Utils.Tuple;

import java.time.LocalTime;
import java.util.*;

public class Line<E>
{
    private String designation;
    private ArrayList<E> stations;

    public Line(String designation) {
        this.designation = designation;
        this.stations = new ArrayList<>();
    }

    public Line(Line<Tuple> line)
    {
        this.designation = line.getDesignation();
        this.stations = new ArrayList<>();
        for(Tuple tuple : line.getStations())
        {
            this.stations.add((E) new Tuple(tuple.station,(LocalTime) tuple.instant.adjustInto(LocalTime.MIDNIGHT)));
        }
    }

    public void addElement(E e)
    {
        if(this.stations.contains(e)) throw new IllegalArgumentException("Estação já existe!");

        this.stations.add(e);
    }

    public String getDesignation() {
        return designation;
    }

    public ArrayList<E> getStations() {
        return stations;
    }

    public void setStations(ArrayList<E> stations) {
        this.stations = stations;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public ArrayList<Station> commonStations(Line line)
    {
        ArrayList<Station> intersect = new ArrayList(this.stations);
        intersect.retainAll(line.getStations());
        return intersect;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Line line = (Line) o;
        return Objects.equals(designation, line.designation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(designation);
    }

}
