package model;

import Utils.Tuple;

import java.time.LocalTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Trip {

    private static final int DEFAULT_ORDER = 1;

    private HashMap<Integer, Line<Tuple>> trips;
    private int order = 1;
    private int totalWeight;
    private LocalTime currTime;

    public Trip(HashMap<Integer, Line<Tuple>> trips, int totalWeight)
    {
        this.trips = trips;
        this.totalWeight = totalWeight;
    }

    public Trip(LocalTime time)
    {
        this.currTime = time;
        this.trips = new HashMap<>();
    }

    public LocalTime getCurrTime()
    {
        return currTime;
    }

    public void setCurrTime(LocalTime currTime)
    {
        this.currTime = currTime;
    }

    public int getTotalWeight()
    {
        return totalWeight;
    }

    public void concatTrip(Trip newTrip)
    {
        if(this.trips.isEmpty()) // se estivermos no inicio do path -> ainda não há nada percorrido
        {
            this.trips = new HashMap<>();
            Line newLine = new Line(newTrip.getTrips().get(1));
            this.trips.put(this.order, newLine);
            this.totalWeight += newTrip.totalWeight;

            // Definimos o instante da primeira estação como sendo o instante original
            this.trips.get(1).getStations().get(0).instant = (LocalTime) this.currTime.adjustInto(trips.get(1).getStations().get(0).instant);
            // Acrescentamos o peso do ramo ao tempo actual
            this.currTime = this.currTime.plusMinutes(newTrip.totalWeight); // tempo actual passa a ser tempoActual+=peso ligaçao
            // Definimos o instante da segunda estação como sendo o novo tempo (tempo anterior + peso ramo)
            this.trips.get(1).getStations().get(1).instant = (LocalTime) this.currTime.adjustInto(trips.get(1).getStations().get(1).instant);
        }
        else if(newTrip.getTrips().keySet().size() == 1)   // se a nova trip só existe em 1 linha
        {
            Line<Tuple> newTripLine = newTrip.getTrips().get(1);
            Line<Tuple> currTripLine = this.trips.get(this.order);
            Tuple newPair = newTripLine.getStations().get(1);       // o que contem a info sobre o novo ponto no caminho
            this.currTime = this.currTime.plusMinutes(newTrip.totalWeight); // tempo actual passa a ser tempoActual+=peso ligaçao
            newPair.instant = (LocalTime) this.currTime.adjustInto(newPair.instant);
            this.totalWeight += newTrip.totalWeight;

            // Se for na mesma linha que a actual
            if(newTripLine.getDesignation().equalsIgnoreCase(currTripLine.getDesignation()))
            {
                currTripLine.getStations().add(newPair);
            }
            else    // Se for numa linha diferente
            {
                Line newLine = new Line(newTripLine.getDesignation());
                newLine.addElement(newPair);
                this.order++;
                this.trips.put(order,newLine);
            }
        }
        else if(newTrip.getTrips().keySet().size() > 1) // esta ligação existe em várias linhas
        {
            Line<Tuple> newTripLine = newTrip.getTrips().get(1);
            Line<Tuple> currTripLine = this.trips.get(this.order);
            Tuple newPair = newTripLine.getStations().get(1);       // o que contem a info sobre o novo ponto no caminho
            this.currTime = this.currTime.plusMinutes(newTrip.totalWeight); // tempo actual passa a ser tempoActual+=peso ligaçao
            newPair.instant = (LocalTime) this.currTime.adjustInto(newPair.instant);
            this.totalWeight += newTrip.totalWeight;

            if(newTrip.trips.values().contains(currTripLine)) // se uma das linhas é a actual
            {
                currTripLine.getStations().add(newPair);
            }
            else    // a linha actual não faz parte das linhas disponiveis
            {       // pegamos so na primeira e metemos essa
                Line newLine = new Line(newTripLine.getDesignation());
                newLine.addElement(newPair);
                this.order++;
                this.trips.put(order,newLine);
            }
        }
    }

    /**
     * Method that creates a Trip that serves only as the container of the information of an edge.
     * @param lineName name of the line in which the 2 connected stations belong to
     * @param station1 first station
     * @param station2 second station
     * @param totalWeight weight of the connection
     * @return
     */
    public static Trip initializeEdgeInfo(String lineName, Station station1, Station station2, double totalWeight)
    {
        HashMap<Integer, Line<Tuple>> trips = new HashMap();
        LocalTime preDef = LocalTime.MIDNIGHT.MIDNIGHT;
        Tuple tuple1 = new Tuple(station1, preDef);           // O instante é 0 porque isto é so a info dum edge, não há instante
        Tuple tuple2 = new Tuple(station2, preDef);          // O instante é 0 porque isto é so a info dum edge, não há instante
        Line line = new Line(lineName);
        line.addElement(tuple1);
        line.addElement(tuple2);
        trips.put(DEFAULT_ORDER, line);

        return new Trip(trips, (int) totalWeight);
    }

    public HashMap<Integer, Line<Tuple>> getTrips() {
        return trips;
    }

    public void addSharedConnection(String lineName)
    {
        order++;
        trips.put(order, new Line<>(lineName));
        Line previous = trips.get(order-1);
        trips.get(order).setStations(previous.getStations()); // definimos como sendo a mesma
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Trip trip = (Trip) o;
        return order == trip.order &&
                Double.compare(trip.totalWeight, totalWeight) == 0 &&
               (trips.keySet().equals(trip.trips.keySet()) && trips.values().equals(trip.trips.values())
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(trips, order, totalWeight);
    }

    @Override
    public String toString()
    {
        String path = "";
        int numStations = 0;
        for(int i = 1; i <= this.order; i++)
        {
            path += "Line: " + this.trips.get(i).getDesignation() + "; \n";
            for(Tuple tuple : this.trips.get(i).getStations())
            {
                path += "Station: " + tuple.station.getDesignation() + "; Time: " + tuple.instant + "; \n";
                numStations++;
            }
            path += "\n";
        }
        path += "\n";
        String station1Name = this.trips.get(1).getStations().get(0).station.getDesignation();
        String station2Name = this.trips.get(this.order).getStations().get(this.trips.get(this.order).getStations().size()-1).station.getDesignation();
        return "Trip " + station1Name + " to " +
                 station2Name +" .\n"
                + path + "Time Elapsed (min): " +
                this.totalWeight + "\nNumber of Station crossed: " + numStations + "\n";
    }
}
