package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class Station
{
    private String designation;
    private double longitude;
    private double latitude;

    public Station(String designation, double longitude, double latitude) {
        this.designation = designation;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public Station(String designation) {
        this.designation = designation;
    }

    public String getDesignation() {
        return designation;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Station station = (Station) o;
        return this.designation.equalsIgnoreCase(station.designation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(designation);
    }

    @Override
    public String toString() {
        return this.getDesignation();
    }

    }
