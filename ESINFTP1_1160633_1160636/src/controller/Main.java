package controller;

import Utils.*;
import model.*;

import java.io.FileNotFoundException;
import java.sql.Array;
import java.sql.SQLOutput;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

public class Main
{
    public static void main(String[] args)
    {

        // Alinea 1 - Inicializar a classe Metro e ler dos ficheiros
        Metro metro = start();
        int count = 0;
        for(Station vertex : metro.getMetroGraph().vertices())
        {
            for(Station vertexAdj : metro.getMetroGraph().adjVertices(vertex))
            {
                if(metro.getMetroGraph().getEdge(vertexAdj, vertex) == null) count++;
            }
        }
        System.out.println("*** Grafo Inicializado ***\nNumVertices: "+metro.getMetroGraph().numVertices()+"\n" +
                "NumEdges: "+metro.getMetroGraph().numEdges());
        System.out.println("Numero de ramos uni-direção = "+count);

        // Alinea 2 - Verificar se o grafo é conexo

        ArrayList<LinkedList> components = GraphAlgorithms.stronglyConnectedComponents(metro.getMetroGraph());
        if(components.size() == 1)
        {
            System.out.println("O grafo é conexo (só tem 1 componente).");
        }
        else
        {
            System.out.println("O grafo não é conexo. Tem " + components.size() + " componentes.");
        }


        // Alinea 4 - Caminho mínimo quanto ao número de estações
        System.out.println("Caminho mínimo de Les Sablons a Saint Michel.\n(Nº Estações)");
        Station temp1 = new Station("Les Sablons");
        Station temp2 = new Station("Saint Michel");
        LinkedList<Station> path = new LinkedList<>();
        Trip totalTrip = metro.shortestTripTime(temp1, temp2, LocalTime.MIDNIGHT, false);
        System.out.println(totalTrip);

        // Alinea 5 - Caminho mínimo quanto
        System.out.println("Caminho mínimo de Les Sablons a Saint Michel.\n(Tempo de Viagem)");
        path.clear();
        totalTrip = metro.shortestTripTime(temp1, temp2, LocalTime.MIDNIGHT, true);
        System.out.println(totalTrip);

        // Alinea 6 - Minimizar o número de mudanças de linha
        System.out.println("Caminho minimo de Les Sablons a Saint Michel.\n(Nº mudanças de linha)");
        path.clear();
        totalTrip = metro.shortestTripTimeLines(temp1,temp2,LocalTime.MIDNIGHT);
        System.out.println(totalTrip);

        // Alinea 7 - Travelling Salesman
        System.out.println("Caminho mínimo de Les Sablons a Saint Michel passando por Cadet e Strasbourg St Denis.\n(Tempo de Viagem)");
        path.clear();
        ArrayList<Station> intermediates = new ArrayList<>();
        Station temp3 = new Station("Cadet");
        Station temp4 = new Station("Strasbourg St Denis");
        intermediates.add(temp4);
        intermediates.add(temp3);
        totalTrip = metro.shortestTripTimeIntermediates(temp1, temp2, intermediates,LocalTime.MIDNIGHT);
        System.out.println(totalTrip);
    }

    public static Metro start()
    {
        // will use default files because null was passed as file name
        return new Metro(null, null, null);
    }

    // Implementação TP1 -----------------------------------------------------------

    /*public static void main(String[] args)
    {
        Metro metro;
        //// Initializes a metro instance and reads data from files
        try {
             metro = start();
        }
        catch(FileNotFoundException ex)
        {
            System.out.println("\n No such file! Make sure the file names/paths in FileReader make sense and run again.");
            return;
        }
        catch(IllegalArgumentException ex)
        {
            System.out.println("\n Illegal Format found in files. Insert files with a valid format.");
            return;
        }
        //// ------------------------------------------------------

        //// Outputs order of stations
        showMetroLine(metro);
        //// -------------------------

        //// Outputs Result of Reading Tickets
        metro.getLine().showArrivalsAndDepartures();
        //// --------------------------------

        //// Outputs the number of passenger that passed each station
        showPassengersPerStation(metro);
        //// --------------------------------------------------------

        showBiggestPossible(metro);

        System.out.println("Tickets that are in transgression:");
        showTransgressions(metro);

    }

    public static Metro start() throws FileNotFoundException
    {
        // will use default files because null was passed as file name
        return new Metro(null, null);
    }

    public static void showMetroLine(Metro metro)
    {
        Iterator it = metro.getLine().getStations().iterator();
        while(it.hasNext())
        {
            Station s = (Station) it.next();
            System.out.println(s.getOrder() + ") - " + s.getDescription());
        }
        System.out.println("\n\n");
    }

    public static void showPassengersPerStation(Metro metro)
    {
        DoublyLinkedList<Station> stations = metro.getLine().getStations();
        int[] passengerRecord = new int[stations.size()];
        metro.getLine().passengersPerStation(passengerRecord);
        for(int i = 0; i < passengerRecord.length; i++)
        {
            System.out.println("Passageiros que passaram na estação " + (i+1) + " :" + passengerRecord[i]);
        }
    }

    public static void showBiggestPossible(Metro metro)
    {
        System.out.println("\nNumber of existing zones: " + metro.getZones().size());

        ArrayList<Tuple> results;
        for(int i = 2; i <= 12; i++)
        {
            System.out.println("\nShowing for z" + i + ":");
            results = metro.kBiggestAdjZones(i);
            System.out.println("Number of different options: " + results.size());
            showIntervals(results, metro.getLine().getStations());
        }
    }

    public static void showTransgressions(Metro metro)
    {
        for(Ticket ticket : metro.findTransgressions())
        {
            System.out.println(ticket);
        }
    }

    //// private utils ---------------------------------------------------

    private static void showIntervals(ArrayList<Tuple> intervals, DoublyLinkedList<Station> stations)
    {
        for(Tuple interval : intervals)
        {
            System.out.println("\nSize of Link :" + (interval.finish - interval.start + 1) + " stations.");
            System.out.println("Starts at: " + interval.start + " Ends at: " + interval.finish);
            ListIterator<Station> it = stations.listIterator();

            while(it.hasNext() && it.nextIndex() < interval.start-1) it.next();
            while(it.hasNext() && it.nextIndex() < interval.finish)
            {
                System.out.print(it.next().getDescription()+" <-> ");
            }

            System.out.println("\n");
        }
    }*/

}
