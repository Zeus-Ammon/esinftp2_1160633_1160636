package Utils;

import model.Line;
import model.Metro;
import model.Station;
import model.Trip;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

public class MetroAlgorithms
{

    public static double shortestPathLines(Metro metro, Station orig, Station dest, LinkedList<Station> totalPath)
    {
        Line lineOrig = chooseLine(metro, orig);
        Line lineDest = chooseLine(metro, dest);
        Graph<Line,Trip> lineGraph = buildLineGraph(metro);
        LinkedList<Line> linePath = new LinkedList<>();
        double numLineChanges = GraphAlgorithms.shortestPath(lineGraph,lineOrig,lineDest,linePath,false);

        LinkedList<Station> inLinePath = new LinkedList<>();
        LinkedList<Station> bestInLinePath = new LinkedList<>();

        ArrayList<Station> commonStations;

        for(int i = 0; i+1 < linePath.size(); i++)
        {
            commonStations = linePath.get(i).commonStations(linePath.get(i+1));
            double shortestPath = Double.POSITIVE_INFINITY;
            Station closestInLine = new Station("Placeholder");

            for(Station shared : commonStations)
            {
                double currPath = shortestPathInLine(metro.getMetroGraph(), linePath.get(i), orig, shared, inLinePath, true);
                if(currPath < shortestPath)
                {
                    shortestPath = currPath;
                    closestInLine = shared;
                    bestInLinePath = new LinkedList<>(inLinePath);
                }
            }

            if( ! totalPath.isEmpty() && totalPath.getLast().equals(bestInLinePath.getFirst()))
            {
                bestInLinePath.removeFirst();
            }
            totalPath.addAll(bestInLinePath);
            orig = closestInLine;

            if(i+1 == linePath.size() - 1) // se for a ultima iteração
            {
                shortestPathInLine(metro.getMetroGraph(), linePath.get(i+1), orig, dest, inLinePath, true);
                if( ! totalPath.isEmpty() && totalPath.getLast().equals(inLinePath.getFirst()))
                {
                    inLinePath.removeFirst();
                }
                totalPath.addAll(inLinePath);
            }

        }

        return numLineChanges;

    }

    public static Graph<Line,Trip> buildLineGraph(Metro metro)
    {
        Graph<Line,Trip> lineGraph =  new Graph<>(false);
        for(Line line : metro.getLines().values())
        {
            for(Line line2 : metro.getLines().values())
            {
                // se não estivermos a comparar uma com ela propria
                if( ! line.getDesignation().equalsIgnoreCase(line2.getDesignation()))
                {
                    // se tiverem elementos em comum --> ou seja partilham estações
                    if( ! Collections.disjoint(line.getStations(), line2.getStations()))
                    {
                        lineGraph.insertEdge(line, line2, null, 0);
                    }
                }
            }
        }
        return lineGraph;
    }

    // Metodo que retorna a linha com maior tamanho que inclui Param: Station s
    public static Line chooseLine(Metro metro, Station s)
    {
        if(metro.getMetroGraph().validVertex(s) == false) return null;

        int maxNumStations = Integer.MIN_VALUE;
        Line biggestLine = new Line("Placeholder");
        for(Line line : metro.getLines().values())
        {
            if(line.getStations().contains(s))
            {
                if(maxNumStations < line.getStations().size())
                {
                    biggestLine = line;
                    maxNumStations = line.getStations().size();
                }
            }
        }
        return biggestLine;
    }

    public static double shortestPathInLine(Graph<Station,Trip> g, Line line, Station vOrig, Station vDest, LinkedList<Station> shortPath, boolean weighted){

        if(! line.getStations().contains(vOrig) || ! line.getStations().contains(vDest))
        {
            return -1;
        }

        // Usamos DFS para obter a lista de Vertices de line na ordem de chegada partindo de vOrig
        LinkedList<Station> vertexList = DepthFirstSearchInLine(g, line, vOrig);
        if(vertexList == null) return -1; // algo correu muito mal. vorig não pertence ao grafo

        // Se a lista não contiver vDest então não é possivel chegar a vDest através de vOrig
        if( ! vertexList.contains(vDest)) return -1;

        // Começar a construir os dados necessários para o Dijkstra
        // 1st Criar e preencher o array de Vertices
        Station[] vertexes = new Station[vertexList.size()];
        int i = 0;											// não podemos aceder à chave de vertex pois usamos o generico V
        // logo usamos um índice criado manualmente
        for(Station vertex : vertexList)
        {
            vertexes[i] = vertex;
            i++;
        }

        // Inicializar array de doubles que vai guardar as distancias com tudo a infinito
        // Inicializar o path (vector de indices que representa o caminho)
        double[] distances = new double[vertexList.size()];
        int[] pathKeys = new int[vertexList.size()];
        distances[0] = 0;		// a distancia de vOrig a ele próprio é 0 ...
        pathKeys[0] = 0;	// alcançamos vOrig indo à primeira posição

        for(i = 1; i < distances.length; i++)
        {
            distances[i] = Double.POSITIVE_INFINITY;
            pathKeys[i] = -1;
        }

        // Inicializar o vector de visitados
        boolean[] visited = new boolean[vertexList.size()]; // inicializa tudo a false já
        visited[0] = true;		// já visitamos o vértice origem

        // Limpar o que possa estar em shortPath já
        shortPath.clear();

        if(weighted)
        {
            // Correr o Dijkstra para ele encontrar os caminhos optimos a partir de vOrig pra todos os outros
            GraphAlgorithms.shortestPathLengthWeighted(g, vOrig, vertexes, visited, pathKeys, distances);
        }
        else
        {
            // Correr o Dijkstra para ele encontrar os caminhos optimos a partir de vOrig pra todos os outros
            GraphAlgorithms.shortestPathLength(g, vOrig, vertexes, pathKeys, distances);
        }


        // Preencher a lista shortPath a partir do pathKeys
        GraphAlgorithms.getPath(g, vOrig, vDest, vertexes, pathKeys, shortPath);

        // retornar a distância ao vértice que nos interessa
        return distances[vertexList.indexOf(vDest)];
    }

    private static void DepthFirstSearchInLine(Graph<Station,Trip> g, Line line, Station vOrig, boolean[] visited, LinkedList<Station> qdfs){

        qdfs.add(vOrig);    // adicionamos o vertice actual à lista
        visited[g.getKey(vOrig)] = true;    // dizemos que ele foi visitado
        for(Station vAdj : g.adjVertices(vOrig))  // por cada vertice adjacente fazemos uma chamada recursiva
        {
            if(!visited[g.getKey(vAdj)] && line.getStations().contains(vAdj))    // desde que ele ainda não tenha sido visitado e seja desta linha
            {
                DepthFirstSearchInLine(g, line, vAdj, visited, qdfs);
            }
        }
    }

    public static LinkedList<Station> DepthFirstSearchInLine(Graph<Station,Trip> g, Line line, Station vert){
        if(g.validVertex(vert) && line.getStations().contains(vert))
        {
            LinkedList<Station> verticesDFS = new LinkedList<>();
            boolean visited[] = new boolean[g.numVertices()];
            DepthFirstSearchInLine(g, line, vert, visited, verticesDFS);

            return verticesDFS;
        }
        else
        {
            return null;
        }
    }
}
