/*
* A collection of graph algorithms.
*/
package Utils;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Stack;

/**
 *
 * @author DEI-ESINF
 */

public class GraphAlgorithms {

    public static<V,E> double travellingSalesman(Graph<V,E> g, V vOrig, V vDest,
                                                 ArrayList<V> intermidiates, LinkedList<V> shortPath)
    {
        LinkedList<V> intermediatePath = new LinkedList<>();
        intermediatePath = new LinkedList<>();
        LinkedList<V> bestIntermediatePath = new LinkedList<>();
        double min = Double.POSITIVE_INFINITY;
        int closestIntermediateIndex = 0;

        for(V vertex : intermidiates)
        {
            double currDist = GraphAlgorithms.shortestPath(g,vOrig,vertex,intermediatePath,true);
            if(currDist < min) // Encontramos um novo intermédio mais proximo
            {
                min = currDist;
                closestIntermediateIndex = intermidiates.indexOf(vertex);
                bestIntermediatePath = (LinkedList) intermediatePath.clone();
            }
        }

        shortPath.addAll(bestIntermediatePath);
        shortPath.remove(shortPath.size() - 1); // removemos o ultimo elemento para ele não se repetir

        if(intermidiates.size() == 1)
        {
            double lastWeight = GraphAlgorithms.shortestPath(g,intermidiates.get(0),vDest,intermediatePath,true);
            shortPath.addAll(intermediatePath);
            return min + lastWeight;
        }

        vOrig = intermidiates.remove(closestIntermediateIndex);
        return min + travellingSalesman(g,vOrig,vDest,intermidiates,shortPath);
    }

    public static<V,E> ArrayList<LinkedList> stronglyConnectedComponents(Graph<V,E> graph)
    {
        boolean[] visitedFirst = new boolean[graph.numVertices()]; // inicializa tudo a false
        Stack<V> stack = new Stack();

        // Primeiro DFS --> mete na stack os vertices de acordo com o finish time
        for(V vertex : graph.vertices())
        {
            if( ! visitedFirst[graph.getKey(vertex)]){
                FirstDepthFirstSearch(graph, vertex, visitedFirst, stack);
            }
        }

        Graph<V,E> reverse = reverseGraph(graph);

        boolean[] visitedSecond = new boolean[reverse.numVertices()]; // inicializa tudo a falso
        ArrayList<LinkedList> sccs = new ArrayList<>(); // arrayList onde guardamos os componentes

        for(V vertex : reverse.vertices())
        {
            while( ! stack.isEmpty())
            {
                V currVertex = stack.pop();
                if( ! visitedSecond[reverse.getKey(currVertex)])
                {
                    LinkedList<V> scc = new LinkedList<>();
                    DepthFirstSearch(reverse, currVertex, visitedSecond, scc);
                    sccs.add(scc);
                }
            }
        }
        return sccs;
    }

    private static<V,E> void FirstDepthFirstSearch(Graph<V,E> g, V vOrig, boolean[] visited, Stack<V> stack){

        visited[g.getKey(vOrig)] = true;    // dizemos que ele foi visitado
        for(V vAdj : g.adjVertices(vOrig))  // por cada vertice adjacente fazemos uma chamada recursiva
        {
            if(!visited[g.getKey(vAdj)])    // desde que ele ainda não tenha sido visitado
            {
                FirstDepthFirstSearch(g, vAdj, visited, stack);
            }
        }

        stack.push(vOrig); // ele mete na stack quando está a recuar pela call stack
    }

    public static<V,E> Graph<V,E> reverseGraph(Graph<V,E> originalGraph)
    {
        Graph<V,E> newGraph = new Graph<>(true); // só dá pra inverter grafos direcionados

        // metemos todos os vertices no novo grafo
        for(V originalVertex : originalGraph.vertices())
            newGraph.insertVertex(originalVertex);

        // metemos todos os ramos mas com a ordem dos vertices trocada
        for(Edge<V,E> originalEdge : originalGraph.edges())
        {
            // Trocamos a ordem dos ramos
            V newVertexOrig = originalEdge.getVDest();
            V newVertexDest = originalEdge.getVOrig();
            newGraph.insertEdge(newVertexOrig, newVertexDest, originalEdge.getElement(), originalEdge.getWeight());
        }

        return newGraph;
    }

    public static<V,E> void connectedComponents(Graph<V,E> g, ArrayList<LinkedList> connectedComps)
    {
        boolean[] visited = new boolean[g.numVertices()]; // inicializa tudo a false

        for(V vertex : g.vertices())    // percorremos os vertices todos
        {
            if( ! visited[g.getKey(vertex)])   // se o vertice ainda não foi visitado
            {
                LinkedList<V> connectedComponent = BreadthFirstSearch(g,vertex);
                connectedComps.add(connectedComponent);
                visited[g.getKey(vertex)] = true;
            }
        }
    }

   /**
   * Performs breadth-first search of a Graph starting in a Vertex 
   * @param g Graph instance
   * @return qbfs a queue with the vertices of breadth-first search 
   */
    public static<V,E> LinkedList<V> BreadthFirstSearch(Graph<V,E> g, V vert){

        if(g.validVertex(vert)) // se for um vértice válido
        {
            boolean[] visited = new boolean[g.numVertices()];
            LinkedList<V> verticesBFS = new LinkedList<>();
            LinkedList<V> aux  = new LinkedList<>();
            verticesBFS.add(vert);
            aux.add(vert);
            visited[g.getKey(vert)] = true; // definimos este vertice como visitado

            while(!aux.isEmpty())
            {
                vert = aux.remove();
                for(Edge<V, E> edge : g.outgoingEdges(vert))
                {
                    V vAdj = g.opposite(vert, edge);
                    int key = g.getKey(vAdj);
                    if(!visited[key])       // se este vertice ainda não tiver sido visitado
                    {
                        verticesBFS.add(vAdj);
                        aux.add(vAdj);
                        visited[key] = true;
                    }
                }
            }
            return verticesBFS;
        }
        else
        {
            return null;
        }
    }

   /**
   * Performs depth-first search starting in a Vertex   
   * @param g Graph instance
   * @param vOrig Vertex of graph g that will be the source of the search
   * @param visited set of discovered vertices
   * @param qdfs queue with vertices of depth-first search
   */
    private static<V,E> void DepthFirstSearch(Graph<V,E> g, V vOrig, boolean[] visited, LinkedList<V> qdfs){

        qdfs.add(vOrig);    // adicionamos o vertice actual à lista
        visited[g.getKey(vOrig)] = true;    // dizemos que ele foi visitado
        for(V vAdj : g.adjVertices(vOrig))  // por cada vertice adjacente fazemos uma chamada recursiva
        {
            if(!visited[g.getKey(vAdj)])    // desde que ele ainda não tenha sido visitado
            {
                DepthFirstSearch(g, vAdj, visited, qdfs);
            }
        }
    }  
  
   /**
   * @param g Graph instance
   * @return qdfs a queue with the vertices of depth-first search 
   */
    public static<V,E> LinkedList<V> DepthFirstSearch(Graph<V,E> g, V vert){
        if(g.validVertex(vert))
        {
            LinkedList<V> verticesDFS = new LinkedList<>();
            boolean visited[] = new boolean[g.numVertices()];
            DepthFirstSearch(g, vert, visited, verticesDFS);

            return verticesDFS;
        }
        else
        {
            return null;
        }
    }
   
    /**
   * Returns all paths from vOrig to vDest
   * @param g Graph instance
   * @param vOrig Vertex that will be the source of the path
   * @param vDest Vertex that will be the end of the path
   * @param visited set of discovered vertices
   * @param path stack with vertices of the current path (the path is in reverse order)
   * @param paths ArrayList with all the paths (in correct order)
   */
    private static<V,E> void allPaths(Graph<V,E> g, V vOrig, V vDest, boolean[] visited, 
                                           LinkedList<V> path, ArrayList<LinkedList<V>> paths){
  
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
   /**
   * @param g Graph instance
   * @return paths ArrayList with all paths from voInf to vdInf 
   */
    public static<V,E> ArrayList<LinkedList<V>> allPaths(Graph<V,E> g, V vOrig, V vDest){
    
        throw new UnsupportedOperationException("Not supported yet.");
    }

	/**
	 * Method that finds the shortest path between two vertices vOrig and vDest.
	 * It uses DFS to find the distances from vOrig to all other vertices. After that it
	 * sets up all data needed to run shortestPathLength (Dijkstra's Algorithm) which finds
	 * the shortest path between the desired vertices.
	 * @param g		The Graph
	 * @param vOrig the origin vertex
	 * @param vDest the destination vertex
	 * @param shortPath the list of vertices in which the shortest path will be saved
	 * @param <V>	the information that represents a vertex
	 * @param <E>	the information that represents an edge
	 * @return		the total weight of the shortest path
	 */
	public static<V,E> double shortestPath(Graph<V,E> g, V vOrig, V vDest, LinkedList<V> shortPath, boolean weighted){

		// Usamos DFS para obter a lista de Vertices na ordem de chegada partindo de vOrig
		LinkedList<V> vertexList = DepthFirstSearch(g, vOrig);
		if(vertexList == null) return -1; // algo correu muito mal. vorig não pertence ao grafo

		// Se a lista não contiver vDest então não é possivel chegar a vDest através de vOrig
		if( ! vertexList.contains(vDest)) return -1;

		// Começar a construir os dados necessários para o Dijkstra
		// 1st Criar e preencher o array de Vertices
		V[] vertexes = (V[]) new Object[g.numVertices()];
		int i = 0;											// não podemos aceder à chave de vertex pois usamos o generico V
															// logo usamos um índice criado manualmente
		for(V vertex : vertexList)
		{
			vertexes[i] = vertex;
			i++;
		}

		// Inicializar array de doubles que vai guardar as distancias com tudo a infinito
		// Inicializar o path (vector de indices que representa o caminho)
		double[] distances = new double[g.numVertices()];
		int[] pathKeys = new int[g.numVertices()];
		distances[0] = 0;		// a distancia de vOrig a ele próprio é 0 ...
		pathKeys[0] = 0;	// alcançamos vOrig indo à primeira posição

		for(i = 1; i < g.numVertices(); i++)
		{
			distances[i] = Double.POSITIVE_INFINITY;
			pathKeys[i] = -1;
		}

		// Inicializar o vector de visitados
		boolean[] visited = new boolean[g.numVertices()]; // inicializa tudo a false já
		visited[0] = true;		// já visitamos o vértice origem

		// Limpar o que possa estar em shortPath já
		shortPath.clear();

        if(weighted)
        {
            // Correr o Dijkstra para ele encontrar os caminhos optimos a partir de vOrig pra todos os outros
            shortestPathLengthWeighted(g, vOrig, vertexes, visited, pathKeys, distances);
        }
        else
        {
            // Correr o Dijkstra para ele encontrar os caminhos optimos a partir de vOrig pra todos os outros
            shortestPathLength(g, vOrig, vertexes, pathKeys, distances);
        }


		// Preencher a lista shortPath a partir do pathKeys
		getPath(g, vOrig, vDest, vertexes, pathKeys, shortPath);

		// retornar a distância ao vértice que nos interessa
		return distances[vertexList.indexOf(vDest)];
	}

    /**
   * Computes shortest-path distance from a source vertex to all reachable 
   * vertices of a graph g with nonnegative edge weights
   * This implementation uses Dijkstra's algorithm
   * @param g Graph instance
   * @param vOrig Vertex that will be the source of the path
   * @param visited set of discovered vertices
   * @param dist minimum distances
   */
    protected static<V,E> void shortestPathLengthWeighted(Graph<V,E> g, V vOrig, V[] vertices,
                                    boolean[] visited, int[] pathKeys, double[] dist){   
        
        int currentVertIndex = 0;	    // começamos no vértice origem
		while(currentVertIndex != -1)
		{
			visited[currentVertIndex] = true;

			for(V vertex : g.adjVertices(vertices[currentVertIndex])) // por cada vértice adjacente ao actual
			{
				double distance = g.getEdge(vertices[currentVertIndex], vertex).getWeight(); // o peso do ramo que os liga

				// Encontrar o índice do vertice adjacente a ser considerado
				int adjIndex = 0;
				for(int i = 0; i < vertices.length; i++)
				{
					if(vertices[i].equals(vertex)) adjIndex = i;
				}

				if ( ! visited[adjIndex] && dist[adjIndex] > dist[currentVertIndex] + distance)
				{
					pathKeys[adjIndex] = currentVertIndex;
					dist[adjIndex] = dist[currentVertIndex] + distance;
				}
			}

			// O indice do proximo vertice a considerar passa a ser daquele que está mais perto
			currentVertIndex = getClosestVertex(dist, visited);
		}
    }

    /**
     * Computes shortest-path distance from a source vertex to all reachable
     * vertices of a graph g with nonnegative edge weights
     * This implementation uses Dijkstra's algorithm
     * @param g Graph instance
     * @param vOrig Vertex that will be the source of the path
     * @param dist minimum distances
     */
    protected static<V,E> void shortestPathLength(Graph<V,E> g, V vOrig, V[] vertices, int[] pathKeys, double[] dist){

        LinkedList<V> aux = new LinkedList<>();
        int currentVertIndex = -1;	// começamos no vértice origem
        aux.add(vOrig);
        while( ! aux.isEmpty())
        {
            V currVertex = aux.remove();
            for(int i = 0; i < g.numVertices(); i++)
            {
                if(vertices[i].equals(currVertex)) currentVertIndex = i;
            }
            for(V vertex : g.adjVertices(currVertex)) // por cada vértice adjacente ao actual
            {
                // Encontrar o índice do vertice adjacente a ser considerado
                int adjIndex = 0;
                for(int i = 0; i < g.numVertices(); i++)
                {
                    if(vertices[i].equals(vertex)) adjIndex = i;
                }

                if ( dist[adjIndex] == Double.POSITIVE_INFINITY )
                {
                    pathKeys[adjIndex] = currentVertIndex;
                    dist[adjIndex] = dist[currentVertIndex] + 1;
                    aux.add(vertex);
                }
            }
        }
    }

    /**
    * Extracts recursively from pathKeys the minimum path between voInf and vdInf
    * The path is constructed from the end to the beginning
    * @param g Graph instance
    * @param path stack with the minimum path (correct order)
    */
    protected static<V,E> void getPath(Graph<V,E> g, V vOrig, V vDest, V[] verts, int[] pathKeys, LinkedList<V> path){
    	// Procuramos e definimos os indices
		int i = 0;
		int srcIndex = -1;
		int destIndex = -1;
		for(V vertex : verts)
		{
			if(vertex.equals(vOrig)) srcIndex = i;

			if(vertex.equals(vDest))destIndex = i;

			i++;
		}

		path.addFirst(vDest);

		if(srcIndex != destIndex) // caso de paragem da recursividade
		{
			destIndex = pathKeys[destIndex];    // andamos a saltar entre indices de vec e indices guardados em vec[]
			getPath(g, vOrig, verts[destIndex], verts, pathKeys, path);
		}
    }

    //shortest-path between voInf and all other
    public static<V,E> boolean shortestPaths(Graph<V,E> g, V vOrig, ArrayList<LinkedList<V>> paths, ArrayList <Double> dists ){
      
        if (!g.validVertex(vOrig)) return false;
        
        int nverts = g.numVertices();
        boolean[] visited = new boolean[nverts]; //default value: false
        int[] pathKeys = new int[nverts];         
        double[] dist = new double [nverts]; 
        V[] vertices = g.allkeyVerts();
    
        for (int i = 0; i < nverts; i++) {
            dist[i] = Double.MAX_VALUE;
            pathKeys[i] = -1;
        }

        shortestPathLengthWeighted(g, vOrig, vertices, visited, pathKeys, dist);
        
        dists.clear(); paths.clear();
        for (int i = 0; i < nverts; i++) {
            paths.add(null);
            dists.add(null);
        }
        for (int i = 0; i < nverts; i++) {
            LinkedList <V> shortPath = new LinkedList<>();
            if (dist[i]!=Double.MAX_VALUE)
                getPath(g,vOrig,vertices[i],vertices,pathKeys,shortPath);                
            paths.set(i, shortPath);
            dists.set(i, dist[i]);
        }
        return true;
    }

	private static int getClosestVertex(double[] dist, boolean[] visited)
	{
		int closestVertIndex = -1;	// se não houver nenhum vertice válido retornamos -1
		double minimumDistance = Double.POSITIVE_INFINITY;
		for(int i = 0; i < dist.length; i++)
		{
			if(dist[i] < minimumDistance && ! visited[i]) // se a distância for menor e ele não tiver sido visitado
			{
				minimumDistance = dist[i];	// nova distancia minima
				closestVertIndex = i;		// novo indice -> do vertice mais próximo
			}
		}
		return closestVertIndex;
	}

    /**
     * Reverses the path
     * @param path stack with path
     */
    private static<V,E> LinkedList<V> revPath(LinkedList<V> path){ 
   
        LinkedList<V> pathcopy = new LinkedList<>(path);
        LinkedList<V> pathrev = new LinkedList<>();
        
        while (!pathcopy.isEmpty())
            pathrev.push(pathcopy.pop());
        
        return pathrev ;
    }    
}