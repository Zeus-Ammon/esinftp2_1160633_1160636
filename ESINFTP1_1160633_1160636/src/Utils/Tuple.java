package Utils;

import model.Station;

import java.time.LocalTime;
import java.util.Objects;

public class Tuple
{
    public Station station;
    public LocalTime instant;

    public Tuple(Station station, LocalTime instant) {
        this.station = station;
        this.instant = instant;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tuple tuple = (Tuple) o;
        return instant == tuple.instant &&
                Objects.equals(station, tuple.station);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(station, instant);
    }
}
