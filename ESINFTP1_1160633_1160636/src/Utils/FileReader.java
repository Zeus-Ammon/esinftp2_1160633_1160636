package Utils;

import model.Line;
import model.Station;
import model.Trip;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class FileReader
{
	// finals para os nomes default de ficheiros
	public static final String COORDINATES_FILE = "coordinates.csv";
	public static final String LINESSTATIONS_FILE = "lines_and_stations.csv";
	public static final String CONNECTIONS_FILE = "connections.csv";

	private static String FILE_SEPARATOR = ";";

	// Finals para coordinates file
	private static int COOR_POS_STATION_NAME = 0;
	private static int COOR_POS_LATITUDE = 1;
	private static int COOR_POS_LONGITUDE = 2;

	// Finals para lines and stations file
	private static int LS_POS_LINE_NAME = 0;
	private static int LS_POS_STATION_NAME = 1;

	// Finals para connections file
	private static int C_POS_LINE_NAME = 0;
	private static int C_POS_STATION1_NAME = 1;
	private static int C_POS_STATION2_NAME = 2;
	private static int C_POS_WEIGHT = 3;

	private String connectionsFile;
	private String linesStationsFile;
	private String coordinatesFile;

	public FileReader(String coordinatesFile, String linesStationsFile, String connectionsFile)
	{
		if(connectionsFile == null || linesStationsFile == null || coordinatesFile == null)
		{
			this.coordinatesFile = COORDINATES_FILE;
			this.linesStationsFile = LINESSTATIONS_FILE;
			this.connectionsFile = CONNECTIONS_FILE;
		}
		else if(connectionsFile.isEmpty() || linesStationsFile.isEmpty() || coordinatesFile.isEmpty())
		{
			throw new IllegalArgumentException("The names of the files must not be empty.");
		}
		else
		{
			this.connectionsFile = connectionsFile;
			this.linesStationsFile = linesStationsFile;
			this.coordinatesFile = coordinatesFile;
		}
	}

	private FileReader(){}	// constructor vazio privado para nunca poder ser usado o publico implicito

	public String getConnectionsFile() {
		return connectionsFile;
	}

	public String getLinesStationsFile() {
		return linesStationsFile;
	}

	public String getCoordinatesFile() {
		return coordinatesFile;
	}

	public void a1ReadFiles(Graph<Station,Trip> graph, HashMap<String, Station> stations, HashMap<String, Line> lines)
	{
		try
		{
			stations = a1_1ReadCoordinates(stations); // cria uma lista auxiliar com as estações criadas com nome e coordenadas
			a1_2ReadLines(stations, lines);	  // cria as linhas com as estações usando a auxList de estações
			a1_3ReadGraph(graph, stations);			 //  cria o grafo em si
		}
		catch (FileNotFoundException e)
		{
			System.out.println("Invalid File Name. " + e.getMessage());
		}
	}

	private HashMap<String, Station> a1_1ReadCoordinates(HashMap<String, Station> stations) throws FileNotFoundException
	{
		Scanner in = new Scanner(new File(this.coordinatesFile));

		String line;
		String[] temp;
		while(in.hasNext())
		{
			line = in.nextLine();
			temp = line.split(FILE_SEPARATOR);

			String name = temp[COOR_POS_STATION_NAME];
			double latitude = Double.parseDouble(temp[COOR_POS_LATITUDE]);
			double longitude = Double.parseDouble(temp[COOR_POS_LONGITUDE]);

			Station station = new Station(name, latitude, longitude);

			if(stations.containsKey(name)) throw new IllegalArgumentException("There cannot be 2 stations with the same name");

			stations.put(name,station);
		}
		in.close();
		return stations;
	}

	private void a1_2ReadLines(HashMap<String, Station> stations, HashMap<String,Line> lines) throws FileNotFoundException
	{
		Scanner in = new Scanner(new File(this.linesStationsFile));

		String line;
		String[] temp;
		String lineName;
		String stationName;
		Line currLine;

		while(in.hasNext())
		{
			// Ler do ficheiro
			line = in.nextLine();
			temp = line.split(FILE_SEPARATOR);
			lineName = temp[LS_POS_LINE_NAME];
			stationName = temp[LS_POS_STATION_NAME];


			if(lines.containsKey(lineName))		// se a linha já existe na lista
			{
				currLine = lines.get(lineName);
			}
			else								// se a linha ainda não existia antes
			{
				currLine = new Line(lineName);
				lines.put(lineName, currLine);
			}

			// Vamos buscar a estação ao mapa das estações e metemos na linha

			if( ! stations.containsKey(stationName)) throw new IllegalArgumentException("Station does not exist.");

			currLine.addElement(stations.get(stationName));
		}
		in.close();
	}

	private void a1_3ReadGraph(Graph<Station,Trip> graph, Map<String, Station> stations) throws FileNotFoundException
	{
		Scanner in = new Scanner(new File(this.connectionsFile));

		String line;
		String[] temp;
		String lineName;
		Station temp1;
		Station temp2;

		while(in.hasNext())
		{
			line = in.nextLine();
			temp = line.split(FILE_SEPARATOR);

			lineName = temp[C_POS_LINE_NAME];

			// Vamos buscar ao map de stations
			temp1 = stations.get(temp[C_POS_STATION1_NAME]);
			temp2 = stations.get(temp[C_POS_STATION2_NAME]);

			double weight = Double.parseDouble(temp[C_POS_WEIGHT]);

			if(graph.getEdge(temp1, temp2) == null) // ainda não existe edge
			{
				Trip edgeInfo = Trip.initializeEdgeInfo(lineName, temp1, temp2, weight);
				graph.insertEdge(temp1, temp2, edgeInfo, weight);
			}
			else	// se já existir ligação entre "A" e "B" então temos de adicionar esta linha
			{
				Edge<Station,Trip> edgeExists = graph.getEdge(temp1,temp2);
				edgeExists.getElement().addSharedConnection(lineName);		// adicionamos esta linha nova ao edge
			}
		}
		in.close();
	}
}

