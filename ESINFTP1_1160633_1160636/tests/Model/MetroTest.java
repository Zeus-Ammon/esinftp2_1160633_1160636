package Model;

import model.Metro;
import model.Station;
import model.Trip;
import org.junit.Test;

import java.time.LocalTime;

import static org.junit.Assert.*;

public class MetroTest
{

    @Test
    public void shortestTripTime()
    {
        String connectionsFile = "testFiles\\testGraphAlgorithms\\shortestPath\\testConnections.csv";
        String linesAndStationsFile = "testFiles\\testGraphAlgorithms\\shortestPath\\testLinesAndStations.csv";
        String coordinatesFile = "testFiles\\testGraphAlgorithms\\shortestPath\\testCoordinates.csv";

        Metro metro = new Metro(coordinatesFile, linesAndStationsFile, connectionsFile);
        Station temp1 = new Station("Vert1");
        Station temp2 = new Station("Vert4");
        LocalTime initialTime = LocalTime.MIDNIGHT; // 00:00
        LocalTime expectedTime = LocalTime.MIDNIGHT.plusMinutes(10);
        LocalTime expectedInstant1 = LocalTime.MIDNIGHT.plusMinutes(4);
        Trip trip = metro.shortestTripTime(temp1,temp2,initialTime,true);
        assertTrue(trip.getTrips().size() == 1);            // so há uma linha
        assertTrue(trip.getTrips().get(1).getStations().size() == 3); // há 3 estações
        assertTrue(trip.getCurrTime().equals(expectedTime));           // passou de 00:00 para 00:10
        assertTrue(trip.getTotalWeight() == 10);              // a viagem demorou 10 minutos

        assertTrue(trip.getTrips().get(1).getStations().get(0).instant.equals(initialTime)); // na 1a estação tempo = inicial
        assertTrue(trip.getTrips().get(1).getStations().get(1).instant.equals(expectedInstant1));   // na 2a tempo = inical+4
        assertTrue(trip.getTrips().get(1).getStations().get(2).instant.equals(expectedTime));       // na 3a tempo = inicial+10
    }


}