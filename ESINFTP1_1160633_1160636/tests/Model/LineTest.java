package Model;

import model.Line;
import model.Station;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class LineTest
{
    @Test
    public void commonStations()
    {

        Line l1 = new Line("linha1");
        Line l2 = new Line("linha2");

        Station s1 = new Station("station1", 10, 20);
        Station s2 = new Station("station2", 20, 40);
        Station s3 = new Station("station3", 30, 60);

        Station s4= new Station("station4", 15, 25);
        Station s5 = new Station("station5", 25, 45);

        l1.addElement(s1);
        l1.addElement(s2);
        l1.addElement(s3);

        l2.addElement(s5);
        l2.addElement(s4);
        l2.addElement(s3);

        ArrayList<Station> expected = new ArrayList();
        expected.add(s3);

        ArrayList<Station> result = l1.commonStations(l2);
        assertEquals(expected,result);

    }

    @Test
    public void equals()
    {
        Line l1 = new Line("linha1");
        Line l2 = new Line("linha2");
        Line l3 = l1;
        Line l4 = new Line("linha1");

        assertTrue(l1.equals(l1));
        assertTrue(l1.equals(l3));
        assertTrue(l1.equals(l4));
        assertFalse(l1.equals(l2));

    }
}