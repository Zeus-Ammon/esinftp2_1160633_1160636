package Model;

import model.Station;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class StationTest
{

    @Test
    public void equals()
    {
        System.out.println("equals");
        Station s1 = new Station("ipo",4, 5);
        Station s2 = new Station("ipo",4, 5);

        assertTrue(s1.equals(s2));
    }

    @Test
    public void notEquals()
    {
        System.out.println("not equals");
        Station s1 = new Station("ipo",1, 2);
        Station s2 = new Station("maia",4, 7);

        assertFalse(s1.equals(s2));
    }

    @Test
    public void getLongitude()
    {
        Station s1 = new Station("ipo",1, 2);

        assertTrue(s1.getLongitude() == 1);
        assertFalse(s1.getLongitude() == 2);
    }

    @Test
    public void getLatitude()
    {
        Station s1 = new Station("ipo",1, 2);

        assertTrue(s1.getLatitude() == 2);
        assertFalse(s1.getLatitude() == 1);
    }

    @Test
    public void toStringTest()
    {
        Station s1 = new Station("ipo",1, 2);

        assertTrue(s1.getDesignation().equals("ipo"));
        assertFalse(s1.getDesignation().equals("maia"));
    }

}