package Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import model.Metro;
import model.Station;
import model.Trip;
import org.junit.Test;
import java.util.ArrayList;
import java.util.LinkedList;
import static org.junit.Assert.*;

    /**
     *
     * @author DEI-ISEP
     */
    public class GraphAlgorithmsTest {

        Graph<String,String> completeMap = new Graph<>(false);
        Graph<String,String> incompleteMap = new Graph<>(false);

        public GraphAlgorithmsTest() {
        }

        @Before
        public void setUp() throws Exception {

            completeMap.insertVertex("Porto");
            completeMap.insertVertex("Braga");
            completeMap.insertVertex("Vila Real");
            completeMap.insertVertex("Aveiro");
            completeMap.insertVertex("Coimbra");
            completeMap.insertVertex("Leiria");

            completeMap.insertVertex("Viseu");
            completeMap.insertVertex("Guarda");
            completeMap.insertVertex("Castelo Branco");
            completeMap.insertVertex("Lisboa");
            completeMap.insertVertex("Faro");

            completeMap.insertEdge("Porto","Aveiro","A1",75);
            completeMap.insertEdge("Porto","Braga","A3",60);
            completeMap.insertEdge("Porto","Vila Real","A4",100);
            completeMap.insertEdge("Viseu","Guarda","A25",75);
            completeMap.insertEdge("Guarda","Castelo Branco","A23",100);
            completeMap.insertEdge("Aveiro","Coimbra","A1",60);
            completeMap.insertEdge("Coimbra","Lisboa","A1",200);
            completeMap.insertEdge("Coimbra","Leiria","A34",80);
            completeMap.insertEdge("Aveiro","Leiria","A17",120);
            completeMap.insertEdge("Leiria","Lisboa","A8",150);

            completeMap.insertEdge("Aveiro","Viseu","A25",85);
            completeMap.insertEdge("Leiria","Castelo Branco","A23",170);
            completeMap.insertEdge("Lisboa","Faro","A2",280);

            incompleteMap = completeMap.clone();

            incompleteMap.removeEdge("Aveiro","Viseu");
            incompleteMap.removeEdge("Leiria","Castelo Branco");
            incompleteMap.removeEdge("Lisboa","Faro");
        }

        /**
         * Test of BreadthFirstSearch method, of class GraphAlgorithms.
         */
        @Test
        public void testBreadthFirstSearch() {
            System.out.println("Test BreadthFirstSearch");

            assertTrue("Should be null if vertex does not exist", GraphAlgorithms.BreadthFirstSearch(completeMap, "LX")==null);

            LinkedList<String> path = GraphAlgorithms.BreadthFirstSearch(incompleteMap, "Faro");

            assertTrue("Should be just one", path.size()==1);

            Iterator<String> it = path.iterator();
            assertTrue("it should be Faro", it.next().compareTo("Faro")==0);

            path = GraphAlgorithms.BreadthFirstSearch(incompleteMap, "Porto");
            assertTrue("Should give seven vertices ", path.size()==7);

            path = GraphAlgorithms.BreadthFirstSearch(incompleteMap, "Viseu");
            assertTrue("Should give 3 vertices", path.size()==3);
        }

        /**
         * Test of DepthFirstSearch method, of class GraphAlgorithms.
         */
        @Test
        public void testDepthFirstSearch() {
            System.out.println("Test of DepthFirstSearch");

            LinkedList<String> path;

            assertTrue("Should be null if vertex does not exist", GraphAlgorithms.DepthFirstSearch(completeMap, "LX")==null);

            path = GraphAlgorithms.DepthFirstSearch(incompleteMap, "Faro");
            assertTrue("Should be just one", path.size()==1);

            Iterator<String> it = path.iterator();
            assertTrue("it should be Faro", it.next().compareTo("Faro")==0);

            path = GraphAlgorithms.DepthFirstSearch(incompleteMap, "Porto");
            assertTrue("Should give seven vertices ", path.size()==7);

            path = GraphAlgorithms.DepthFirstSearch(incompleteMap, "Viseu");
            assertTrue("Should give 3 vertices", path.size()==3);

            it = path.iterator();
            assertTrue("First in visit should be Viseu", it.next().compareTo("Viseu")==0);
            assertTrue("then Guarda", it.next().compareTo("Guarda")==0);
            assertTrue("then Castelo Branco", it.next().compareTo("Castelo Branco")==0);
        }

        @Test
        public void stronglyConnectedComponents()
        {
            String connectionsFile = "testFiles\\testGraphAlgorithms\\sccs\\testConnections.csv";
            String linesAndStationsFile = "testFiles\\testGraphAlgorithms\\sccs\\testLinesAndStations.csv";
            String coordinatesFile = "testFiles\\testGraphAlgorithms\\sccs\\testCoordinates.csv";

            Metro metro = new Metro(coordinatesFile, linesAndStationsFile, connectionsFile);
            ArrayList<LinkedList> sccs = GraphAlgorithms.stronglyConnectedComponents(metro.getMetroGraph());
            assertTrue(sccs.size() == 4); // numero de componentes encontrados
        }

        @Test
        public void shortestPathWeighted()
        {
            String connectionsFile = "testFiles\\testGraphAlgorithms\\shortestPath\\testConnections.csv";
            String linesAndStationsFile = "testFiles\\testGraphAlgorithms\\shortestPath\\testLinesAndStations.csv";
            String coordinatesFile = "testFiles\\testGraphAlgorithms\\shortestPath\\testCoordinates.csv";

            Metro metro = new Metro(coordinatesFile, linesAndStationsFile, connectionsFile);
            Graph<Station, Trip> graph = metro.getMetroGraph();
            Station temp1 = new Station("Vert1");
            Station temp2 = new Station("Vert4");
            LinkedList<Station> path = new LinkedList<>();
            double weightPath = GraphAlgorithms.shortestPath(graph,temp1,temp2,path,true);
            assertTrue(weightPath == 10);
            assertTrue(path.size() == 3);

        }

        @Test
        public void shortestPath()
        {
            String connectionsFile = "testFiles\\testGraphAlgorithms\\shortestPath\\testConnections.csv";
            String linesAndStationsFile = "testFiles\\testGraphAlgorithms\\shortestPath\\testLinesAndStations.csv";
            String coordinatesFile = "testFiles\\testGraphAlgorithms\\shortestPath\\testCoordinates.csv";

            Metro metro = new Metro(coordinatesFile, linesAndStationsFile, connectionsFile);
            Graph<Station, Trip> graph = metro.getMetroGraph();
            Station temp1 = new Station("Vert1");
            Station temp2 = new Station("Vert4");
            LinkedList<Station> path = new LinkedList<>();
            double weightPath = GraphAlgorithms.shortestPath(graph,temp1,temp2,path,false);
            assertTrue(weightPath == 2);
            assertTrue(path.size() == 3);
        }

        @Test
        public void shortestPath2()
        {
            Metro metro = new Metro(null, null, null);
            Graph<Station, Trip> graph = metro.getMetroGraph();
            Station temp1 = new Station("Termes");
            Station temp2 = new Station("Boissiere");
            LinkedList<Station> path = new LinkedList<>();
            double weightPath = GraphAlgorithms.shortestPath(graph,temp1,temp2,path,false);
            assertTrue(weightPath == 3);
            assertTrue(path.size() == 4);
        }

        @Test
        public void travellingSalesman()
        {
            Station temp1 = new Station("Les Sablons");
            Station temp2 = new Station("Saint Michel");
            LinkedList<Station> pathTotal = new LinkedList<>();
            LinkedList<Station> path1 = new LinkedList<>();
            LinkedList<Station> path2 = new LinkedList<>();

            ArrayList<Station> intermediates = new ArrayList<>();
            Station temp3 = new Station("Strasbourg St Denis");
            intermediates.add(temp3);

            Metro metro = new Metro(null, null, null);
            Graph<Station, Trip> graph = metro.getMetroGraph();

            double weightPath = GraphAlgorithms.travellingSalesman(graph,temp1,temp2,intermediates,pathTotal);

            double weight1 = GraphAlgorithms.shortestPath(graph,temp1,intermediates.get(0),path1,true);
            double weight2 = GraphAlgorithms.shortestPath(graph,intermediates.get(0),temp2,path2,true);

            assertTrue(weightPath == (weight1 + weight2));
            assertTrue(pathTotal.size() == (path1.size() + path2.size() - 1));
        }

        @Test
        public void travellingSalesman2()
        {
            Station temp1 = new Station("Les Sablons");
            Station temp2 = new Station("Saint Michel");
            LinkedList<Station> pathTotal = new LinkedList<>();
            LinkedList<Station> path1 = new LinkedList<>();
            LinkedList<Station> path2 = new LinkedList<>();
            LinkedList<Station> path3 = new LinkedList<>();

            ArrayList<Station> intermediates = new ArrayList<>();
            Station temp3 = new Station("Strasbourg St Denis");
            Station temp4 = new Station("Cadet");
            intermediates.add(temp3);
            intermediates.add(temp4);

            Metro metro = new Metro(null, null, null);
            Graph<Station, Trip> graph = metro.getMetroGraph();

            double weight1 = GraphAlgorithms.shortestPath(graph,temp1,intermediates.get(1),path1,true);
            double weight2 = GraphAlgorithms.shortestPath(graph,intermediates.get(1),intermediates.get(0),path2,true);
            double weight3 = GraphAlgorithms.shortestPath(graph,intermediates.get(0),temp2,path3,true);

            double weightPath = GraphAlgorithms.travellingSalesman(graph,temp1,temp2,intermediates,pathTotal);

            assertTrue(weightPath == (weight1 + weight2 +weight3));
            assertTrue(pathTotal.size() == (path1.size() + path2.size() + path3.size() - 2));
        }


}