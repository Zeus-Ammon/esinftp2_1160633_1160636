package Utils;

import model.Metro;
import model.Station;
import model.Trip;
import org.junit.Test;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.LinkedList;

import static org.junit.Assert.*;

public class MetroAlgorithmsTest {

    @Test
    public void shortestPath()
    {
        String connectionsFile = "testFiles\\testMetroAlgorithms\\testConnections.csv";
        String coordinatesFile = "testFiles\\testMetroAlgorithms\\testCoordinates.csv";
        String linesStationsFile = "testFiles\\testMetroAlgorithms\\testLinesAndStations.csv";

        Station temp1 = new Station("Vert1");
        Station temp2 = new Station("Vert4");
        LinkedList<Station> pathTotal = new LinkedList<>();

        Metro metro = new Metro(coordinatesFile, linesStationsFile, connectionsFile);

        double weightPath = MetroAlgorithms.shortestPathLines(metro,temp1,temp2,pathTotal);

        assertTrue(weightPath == 0);
    }

    @Test
    public void shortestPath2()
    {
        Station temp1 = new Station("Les Sablons");
        Station temp2 = new Station("Saint Michel");
        LinkedList<Station> pathTotal = new LinkedList<>();

        Metro metro = new Metro(null, null, null);

        double weightPath = MetroAlgorithms.shortestPathLines(metro,temp1,temp2,pathTotal);
        assertTrue(weightPath == 1);
        Trip totalTrip = metro.shortestTripTimeLines(temp1,temp2, LocalTime.MIDNIGHT);
    }
}