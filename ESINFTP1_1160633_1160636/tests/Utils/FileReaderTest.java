package Utils;

import model.Line;
import model.Metro;
import model.Station;
import model.Trip;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import static org.junit.Assert.*;

public class FileReaderTest {

    @Test
    public void a1ReadFiles()
    {
        String testConnections = "testFiles\\testFileReader\\testConnections.csv";
        String testCoordinates = "testFiles\\testFileReader\\testCoordinates.csv";
        String testLinesAndStations = "testFiles\\testFileReader\\testLinesAndStations.csv";

        Metro metro = new Metro(testCoordinates, testLinesAndStations, testConnections);
        Graph<Station,Trip> graph = metro.getMetroGraph();

        // Vertices que tem de ser criados
        Station dummy1 = new Station("A");
        Station dummy2 = new Station("B");
        Station dummy3 = new Station("C");
        Station dummy4 = new Station("D");

        // Verificar que o hashMap "allStations" em Metro tem todas as estações
        assertTrue(metro.getAllStations().size() == 4);
        assertTrue(metro.getAllStations().containsKey(dummy1.getDesignation()));
        assertTrue(metro.getAllStations().containsKey(dummy2.getDesignation()));
        assertTrue(metro.getAllStations().containsKey(dummy3.getDesignation()));
        assertTrue(metro.getAllStations().containsKey(dummy4.getDesignation()));

        // Verificar que o grafo contem os vertices correctos
        assertTrue(graph.numVertices() == 4);
        assertTrue(graph.validVertex(dummy1));
        assertTrue(graph.validVertex(dummy2));
        assertTrue(graph.validVertex(dummy3));
        assertTrue(graph.validVertex(dummy4));

        // Verificar que o grafo tem os edges correctos
        Edge<Station,Trip> edge1 = graph.getEdge(dummy1,dummy2);
        Edge<Station,Trip> edge2 = graph.getEdge(dummy1,dummy3);
        Edge<Station,Trip> edge3 = graph.getEdge(dummy3,dummy4);
        Edge<Station,Trip> edge4 = graph.getEdge(dummy2,dummy4);
        assertTrue(graph.numEdges() == 5);
        assertNotNull(edge1);
        assertNotNull(edge2);
        assertNotNull(edge3);
        assertNotNull(edge4);
        assertTrue(edge1.getWeight() == 2);
        assertTrue(edge2.getWeight() == 4);
        assertTrue(edge3.getWeight() == 5);
        assertTrue(edge4.getWeight() == 10);
        assertEquals(edge1.getVOrig(), dummy1);
        assertEquals(edge1.getVDest(), dummy2);
        assertEquals(edge2.getVOrig(), dummy1);
        assertEquals(edge2.getVDest(), dummy3);
        assertEquals(edge3.getVOrig(), dummy3);
        assertEquals(edge3.getVDest(), dummy4);
        assertEquals(edge4.getVOrig(), dummy2);
        assertEquals(edge4.getVDest(), dummy4);

        Edge<Station,Trip> edgeMulLines = graph.getEdge(dummy1,dummy2);
        assertTrue(edgeMulLines.getElement().getTrips().size() == 2); // duas linhas passam por esta ramo

        /// ------- Testar Excepções
        boolean exception = false;
        try
        {
            FileReader reader2 = new FileReader("","","");
        }
        catch(IllegalArgumentException e)
        {
            exception = true;
        }
        assertTrue(exception);

        FileReader reader3 = new FileReader(null, null, null);
        assertTrue(reader3.getConnectionsFile().equalsIgnoreCase(FileReader.CONNECTIONS_FILE));
        assertTrue(reader3.getCoordinatesFile().equalsIgnoreCase(FileReader.COORDINATES_FILE));
        assertTrue(reader3.getLinesStationsFile().equalsIgnoreCase(FileReader.LINESSTATIONS_FILE));

        ////
        exception = false;
        FileReader reader4 = new FileReader("noSuchFile", "noSuchFile", "noSuchFile");
        Graph<Station,Trip> testGraph = new Graph<>(true);
        HashMap<String, Station> hash1 = new HashMap<>();
        HashMap<String, Line> hash2 = new HashMap<>();
        reader4.a1ReadFiles(testGraph, hash1, hash2);
        assertTrue(testGraph.numVertices() == 0);
        assertTrue(testGraph.numEdges() == 0);
        assertTrue(hash1.isEmpty());
        assertTrue(hash2.isEmpty());
    }

    @Test
    public void checkNoEqualEdgesWithDifferentWeights() throws FileNotFoundException
    {
        System.out.println(new File("").getAbsolutePath().toString());
        String line = "a";
        String line2;
        String[] temp;
        String[] temp2;
        int count = 0;
        for(int i = 1; i <= 712; i++)
        {
            int innerCount = 0;
            Scanner in = new Scanner(new File("connections.csv"));

            while(innerCount != i){ line = in.nextLine(); innerCount++; }

            temp = line.split(";");
            String lineName = temp[0];
            String info = temp[1]+temp[2];
            String weight = temp[3];

            while(in.hasNext())
            {
                line2 = in.nextLine();
                temp2 = line2.split(";");
                String lineName2 = temp[0];
                String info2 = temp2[1] + temp2[2];
                String weight2 = temp2[3];

                if( (! lineName.equalsIgnoreCase(lineName2)) &&
                     info.equalsIgnoreCase(info2)            &&
                        (! weight.equalsIgnoreCase(weight2)))
                {
                    count++;
                }
            }

        }
        assertTrue(count == 0);
    }
}